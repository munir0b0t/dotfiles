# Makefile to copy the files to home directory

.PHONY: all bashfiles gitfiles vimfiles gvimfiles

all: bashfiles gitfiles vimfiles bin

bashfiles: bashrc bash_profile

vimfiles: vim vimrc

gvimfiles: vimfiles gvimrc

bin:
	mkdir -pv $(HOME)/.local/bin
	ln -sv .local/bin $(HOME)/bin
	cp -v local/bin/* $(HOME)/.local/bin/

vim:
	mkdir -pv $(HOME)/.vim/{colors,syntax}
	cp -v vim/colors/custom.vim $(HOME)/.vim/colors/
	cp -v vim/syntax/* $(HOME)/.vim/syntax/

vimrc:
	cp -v vimrc $(HOME)/.vimrc

gvimrc:
	cp -v gvimrc $(HOME)/.gvimrc

bashrc:
	cp -v bashrc $(HOME)/.bashrc

bash_profile:
	cp -v bash_profile $(HOME)/.bash_profile
	ln -sv .bash_profile $(HOME)/.profile

gitfiles:
	mkdir -pv $(HOME)/.config/git
	cp -Rv config/git/* $(HOME)/.config/git/

xfceconfig:
	mkdir -pv $(HOME)/.config/xfce4
	cp -Rv config/xfce4/* $(HOME)/.config/xfce4
